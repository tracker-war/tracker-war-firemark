-module(tracker_log).
-behaviour(gen_server).

-export([start_link/2]).
-export([send/2]).
-export([init/1, terminate/2, handle_cast/2, handle_call/3]).
-record(state, {name, file}).

start_link(Name, Filepath) ->
	gen_server:start_link({local, Name}, ?MODULE, [Name, Filepath], []).

send(Name, Message) ->
  gen_server:cast(Name, Message).

init([Name, Filepath]) ->
  io:fwrite("Init ~s...~n", [Name]),
  {ok, File} = file:open(Filepath, [delayed_write, append]),
  {ok, #state{name=Name, file=File}}.

terminate(Reason, State) ->
  io:fwrite("Closing [~p] ~s...~n", [Reason, State#state.name]),
  file:close(State#state.file),
  ok.

handle_cast([Cip, Time], State) ->
  Line = io_lib:format("~s ~s~n", [Cip, Time]),
  ok = file:write(State#state.file, Line),
  {noreply, State};

handle_cast(Message, State) ->
  io:fwrite("WRONG MESSAGE ~w", [Message]),
  {noreply, State}.

handle_call(_Message, _From, State) ->
  {noreply, State}.
