PROJECT = tracker_war
PROJECT_DESCRIPTION = TrackerWar
PROJECT_VERSION = 0.1.0

DEPS = cowboy eredis
dep_cowboy_commit = 2.3.0

DEP_PLUGINS = cowboy

include erlang.mk
