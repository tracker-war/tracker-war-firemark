-module(tracker_handler).
-behavior(cowboy_handler).

-export([init/2]).

init(Req, State) ->
  QsMap = cowboy_req:match_qs([cip, {send_image, int, 1}], Req),
  #{cip := Cip, send_image := SendImage} = QsMap,
  StrCip = binary:bin_to_list(Cip),
  send_data(StrCip),
  {Code, Headers, Response} = get_content(#{send_image => SendImage}),
  NewReq = cowboy_req:reply(Code, Headers, Response, Req),
	{ok, NewReq, State}.

send_data(Cip) ->
  LogArgs = [Cip, get_timestamp()],
  tracker_log:send(tracker_log, LogArgs),
  tracker_queue:send(tracker_queue, LogArgs).

get_timestamp() ->
  TS = {_, _, Micro} = os:timestamp(),
  {Date, Time} = calendar:now_to_universal_time(TS),
  {Year, Month, Day} = Date,
  {Hour, Minute, Second} = Time,
  io_lib:format("~4w-~2..0w-~2..0w ~2..0w:~2..0w:~2..0w.~6..0w", [
    Year, Month, Day, Hour, Minute, Second, Micro]).

get_content(#{send_image := 0}) ->
  {204, #{}, <<"">>};

get_content(_) ->
  Headers = #{<<"content-type">> => "image/gif"},
  {200, Headers, <<"IMAGE">>}.
