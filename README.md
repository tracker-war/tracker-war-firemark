# Command

## Build

```bash
docker-compose build 
```

## Run

```bash
docker-compose up -d
```

## Load logs

```bash
cat logs/log.log | docker-compose exec -T tracker escript scripts/put_logs tracker@tracker.com tracker
```

## Redis panel

```bash
docker-compose run --rm redis redis-cli -h redis -p 6379
```


# Benchmark

## wrk and tracker on one machine

### send_image=1

```
Running 30s test @ http://localhost:7767/?cip=1234567810&send_image=1
  4 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     6.40ms    4.35ms 224.37ms   78.89%
    Req/Sec     4.12k   468.18     6.68k    73.00%
  492414 requests in 30.04s, 56.91MB read
Requests/sec:  16391.23
Transfer/sec:      1.89MB
```

### send_image=0

```
Running 30s test @ http://localhost:7767/?cip=1234567810&send_image=0
  4 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     6.30ms    4.06ms 212.51ms   76.94%
    Req/Sec     4.18k   514.14     7.12k    73.25%
  499210 requests in 30.05s, 38.18MB read
Requests/sec:  16612.64
Transfer/sec:      1.27MB
```


## wrk and tracker on another machines (wifi connection)

### send_image=1


```
Running 30s test @ http://192.168.1.216:7767/?cip=1234&send_image=1
  4 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    74.82ms   12.79ms 165.67ms   88.97%
    Req/Sec   331.46     60.42   555.00     83.46%
  39559 requests in 30.06s, 4.57MB read
Requests/sec:   1315.98
Transfer/sec:    155.69KB
```

### send_image=0

```
Running 30s test @ http://192.168.1.216:7767/?cip=1234&send_image=0
  4 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    75.65ms   13.09ms 377.86ms   90.14%
    Req/Sec   327.90     60.60   560.00     82.80%
  39162 requests in 30.07s, 2.99MB read
Requests/sec:   1302.55
Transfer/sec:    101.95KB
```


## nginx + 2×tracker (on local machine)

```
Running 30s test @ http://localhost:7767/?cip=1234567810&send_image=1
  4 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     6.52ms    4.26ms  75.33ms   77.00%
    Req/Sec     4.05k   524.68     5.72k    74.89%
  479651 requests in 30.02s, 55.44MB read
  Socket errors: connect 0, read 8447, write 0, timeout 0
Requests/sec:  15975.31
Transfer/sec:      1.85MB
```


## haproxy 2×tracker (on local machine)

```
Running 30s test @ http://localhost:7767/?cip=1234567810&send_image=1
  4 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    13.74ms    6.79ms 108.04ms   77.05%
    Req/Sec     1.87k   215.38     2.38k    76.25%
  223647 requests in 30.05s, 25.81MB read
Requests/sec:   7442.62
Transfer/sec:      0.86MB
```
