FROM erlang:21.0

RUN mkdir -p /code
WORKDIR /code

COPY Makefile .
COPY erlang.mk .

RUN make deps

COPY rel ./rel
COPY src ./src
COPY scripts ./scripts
COPY relx.config .

RUN mkdir logs
RUN make rel

CMD _rel/tracker/bin/tracker
#ENTRYPOINT ["_rel/tracker/bin/tracker"]
