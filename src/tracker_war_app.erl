-module(tracker_war_app).
-behaviour(application).

-export([start/2]).
-export([stop/1]).

start(_Type, _Args) ->
  Dispatch = dispatch(),
  {ok, _} = cowboy:start_clear(
    tracker_listener, 
    [{port, 7767}],
    #{env => #{dispatch => Dispatch}}
  ),
	tracker_war_sup:start_link().

dispatch() ->
  cowboy_router:compile([
    {'_', [{"/", tracker_handler, []}]}
  ]).

stop(_State) ->
	ok.
