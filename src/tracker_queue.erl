-module(tracker_queue).
-behaviour(gen_server).

-export([start_link/2]).
-export([send/2]).
-export([init/1, terminate/2, handle_cast/2, handle_call/3]).
-record(state, {name, redis}).

start_link(Name, Conf) ->
  gen_server:start_link({local, Name}, ?MODULE, [Name, Conf], []).

send(Name, Message) ->
  gen_server:cast(Name, Message).

init([Name, [Host, Port, DatabaseNum]]) ->
  io:fwrite("Init ~s...~n", [Name]),
  case eredis:start_link(Host, Port, DatabaseNum) of
    {ok, Redis} ->
      {ok, #state{name=Name, redis=Redis}};
    {error, Reasons} -> 
      io:fwrite("REDIS ERR: ~w~n", [Reasons]),
      {error, Reasons}
  end.

terminate(_Reason, State) ->
  io:fwrite("Closing ~s...~n", [State#state.name]),
  ok.

handle_cast([Cip, Time], State) ->
  Commands = [
    ["INCR",  Cip ++ ":count"],
    ["LPUSH", Cip ++ ":list", Time]
  ],
  eredis:qp(State#state.redis, Commands),
  {noreply, State};

handle_cast(_Message, State) ->
  io:write("WRONG MESSAGE"),
  {noreply, State}.

handle_call(_Message, _From, State) ->
  {noreply, State}.
