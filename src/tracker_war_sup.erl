-module(tracker_war_sup).
-behaviour(supervisor).

-export([start_link/0]).
-export([init/1]).

start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
  TrackerLogPath = os:getenv("TRACKER_LOG_PATH", "/code/logs/log.log"),
  RedisHost = os:getenv("TRACKER_REDIS_HOST", "redis"),
  {RedisPort, _} = string:to_integer(os:getenv("TRACKER_REDIS_PORT", "6379")),
  {RedisDb, _} = string:to_integer(os:getenv("TRACKER_REDIS_DB", "0")),

  Flags = #{strategy => one_for_one, intensity => 1, period => 5},
	Procs = [
    #{
      id => tracker_log,
      start => {tracker_log, start_link,
                [tracker_log, TrackerLogPath]}},
    #{
      id => tracker_queue,
      start => {tracker_queue, start_link,
                [tracker_queue, [RedisHost, RedisPort, RedisDb]]}}
  ],
	{ok, {Flags, Procs}}.
